# Football
## Quick start
Run on IOS
```sh
1. Install npm dependences: 'yarn install'
1. Install cocoapods files: run 'cd ./ios', then 'pod install'
3. Run 'yarn start' in the root project
4. Open another console tab and run 'react-native run-ios'
```

Run on Android
```sh
1. Run 'yarn install'
2. Run 'yarn start'
3. Open another console tab and run 'react-native run-android'
```

## Demo
<p align="center">
    <img src='demo.gif' width='600' alt='npm start'>
</p>
