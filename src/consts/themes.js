export const fonts = {
  regular: 'Muli-Regular', // using
  italic: 'JosefinSans-Italic',
  light: 'JosefinSans-Light',
  lightItalic: 'JosefinSans-LightItalic',
  semibold: 'Muli-SemiBold',
  semiboldItalic: 'JosefinSans-SemiBoldItalic',
  bold: 'Muli-Bold', // usign
  boldItalic: 'JosefinSans-BoldItalic',
  thin: 'JosefinSans-Thin',
  thinItalica: 'JosefinSans-ThinItalic',
};

export const colors = {
  primary: '#1883C6',
  primaryLighten: '#f2424b',
  facebookBLue: '#3b5998',
  googleRed: '#db3236',
  white: '#fff',
  gray: '#454545',
  darkGray: '#464546',
  gainsboro: '#d8d8d8',
  danger: '#FF151F',
  persianRed: '#DF2C2C',
  success: '#4ECE3D',
  disabled: '#b5b5b5',
  lime: '#c5ffe2',
  grayish: '#efe7e7',
  nobel: '#9B9B9B',
  nobelLight: '#a2a2a2',
  darkBlue: '#1875af',
  lightBlue: '#40ACEF',
  greenChart: '#1ABF5F',
  orangeChart: '#D5891B',
  blueChart: '#1883C6',
  grayChart: '#ACACAC',
};

export const offsets = {
  extraSmall: 5,
  small: 10,
  medium: 15,
  big: 20,
  extraBig: 25,
  large: 30,
  extraLarge: 35,
};

export const colorsForCharts = [
  colors.blueChart,
  colors.greenChart,
  colors.orangeChart,
  colors.grayChart,
];

