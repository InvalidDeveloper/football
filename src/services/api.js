import apisauce from 'apisauce'

const create = (baseURL = 'http://api.football-data.org/v2') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'content-type': 'application/json',
      'X-Auth-Token': 'd80acd5ebbd14db39c176271ba12e3dc',
    },
    timeout: 5000,
  })

  const teams = () => {
    return api.get('/teams')
  }

  const teamById = (id) => {
    return api.get(`/teams/${id}`)
  }

  const matchesById = ({params = {}, id}) => {
    const tParams = Object.keys(params).reduce((acc, next, idx) => {
      if (idx === 0) {
        return `${acc}?${next}=${params[next]}`
      }
      return `${acc}&${next}=${params[next]}`
    }, '')
    return api.get(`/teams/${id}/matches${tParams}`)
  }
  return {
    teams,
    teamById,
    matchesById,
  }
}

export default {create}
