import React from 'react'
import { createStackNavigator } from 'react-navigation'
import {
  HOME_SCREEN,
  TEAM_SCREEN,
} from 'Consts/screenNames'

import HomeScreen from 'Screens/HomeScreen'
import TeamInfoScreen from 'Screens/TeamInfoScreen/TeamInfoScreeen'

import FootballHeader from 'Components/footballHeader'

import { defaultConfig } from './configs'

const AppStack = createStackNavigator({
  [HOME_SCREEN]: {
    screen: HomeScreen,
    navigationOptions: (props) => {
      const {navigation} = props
      return {
        headerTitle: <FootballHeader navigation={navigation}/>,
      }
    }
  },
  [TEAM_SCREEN]: {
    screen: TeamInfoScreen,
    navigationOptions: {
      headerTitle: TEAM_SCREEN,
    }
  },
}, {
  initialRouteName: HOME_SCREEN,
  defaultNavigationOptions: defaultConfig,
})

export default AppStack
