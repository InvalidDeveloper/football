import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { View } from "react-native"
import { FootballActions } from 'Redux/reducers/footballReducer'
import ListTeams from 'Components/listTeams/listTeams'
import { TeamActions } from 'Redux/reducers/teamReducer'

const HomeScreen = () => {
  const dispatch = useDispatch()
  const teams = useSelector(state => state.footballReducer.teams)
  useEffect(() => {
    dispatch(FootballActions.getTeams())
  }, [])
  return (
    <View style={{flex:1}}>
      <ListTeams onPress={(item) => dispatch(TeamActions.getTeamInfo(item))} data={teams}/>
    </View>
  )
}

export default HomeScreen
