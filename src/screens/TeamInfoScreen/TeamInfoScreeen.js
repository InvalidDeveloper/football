import React from 'react'
import { useSelector } from 'react-redux'
import { View } from 'react-native'
import TeamList from 'Components/teamList'

const TeamInfoScreen = () => {
  const squad = useSelector(state => state.teamReducer.squad)
  const matches = useSelector(state => state.teamReducer.matches)
  return (
    <View style={{flex: 1}}>
      <TeamList
        squad={squad}
        matches={matches}
      />
    </View>
  )
}

export default TeamInfoScreen
