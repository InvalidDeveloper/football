import React, { useCallback, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { APP_STACK, LOGIN_STACK } from 'Consts/screenNames'
import SplashScreen from 'react-native-splash-screen'

const AppLoadingScreen = (props) => {
  const token = useSelector(state => state.userReducer.token)
  const bootstrap = useCallback(() => {
    let routeName = LOGIN_STACK
    if (token) {
      routeName = APP_STACK
    }

    props.navigation.navigate(routeName)
  }, [token])

  useEffect(() => {
    bootstrap()
  }, [token])

  useEffect(() => {
    SplashScreen.hide()
  }, [])
  return null
}

export default AppLoadingScreen
