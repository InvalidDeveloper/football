import { StyleSheet } from 'react-native'
import { colors } from 'Consts/themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20
  },
})
