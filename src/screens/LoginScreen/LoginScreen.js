import React from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'
import { UserActions } from 'Redux/reducers/userReducer'
import { LOGIN } from 'Consts/screenNames'
import { Button, Text } from 'native-base';
import styles from './styles'

const LoginScreen = () => {
  const dispatch = useDispatch()
  return (
    <View style={styles.container}>
      <Button
        rounded
        block
        onPress={() => dispatch(UserActions.login())}>
        <Text>{LOGIN}</Text>
      </Button>
    </View>
  )
}

export default LoginScreen
