import React  from 'react'
import { View, Text } from 'react-native'
import TouchableView from 'Components/touchableView'
import { FOOTBALL, LOGOUT } from 'Consts/screenNames'
import { useDispatch } from 'react-redux'
import styles from './styles'
import { UserActions } from 'Redux/reducers/userReducer'

const FootballHeader = (props) => {
  const dispatch = useDispatch()
  return (
    <View style={styles.container}>
      <View style={styles.emptyElement}>

      </View>
      <View style={styles.headerElement}>
        <Text style={styles.header}>
          {FOOTBALL}
        </Text>
      </View>
      <TouchableView
        style={styles.logoutElement}
        onPress={() => dispatch(UserActions.logout())}>
        <Text style={styles.back}>
          {LOGOUT}
        </Text>
      </TouchableView>
    </View>
  )
}

export default FootballHeader
