import { StyleSheet } from 'react-native'
import { colors } from 'Consts/themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.primary,
    justifyContent: 'space-between',
    paddingRight: 10,
  },
  headerElement: {
    flex: 1,
    alignItems: 'center',
  },
  emptyElement: {
    flex: 1,
    alignItems: 'flex-start',
  },
  logoutElement: {
    flex: 1,
    alignItems: 'flex-end',
  },
  header: {
    color: 'white',
    fontSize: 17,
    fontWeight: '600',
  },
  back: {
    color: 'white',
  }
})
