import { StyleSheet } from 'react-native'
import { colors } from 'Consts/themes'

export default StyleSheet.create({
  text: {
    color: colors.darkGray,
  }
})
