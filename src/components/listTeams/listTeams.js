import React from 'react'
import { Container, Content, List, ListItem, Left, Right, Text } from 'native-base'
import styles from './styles'


const ListTeams = (props) => {
  return (
    <Container>
      <Content>
        <List>
          {props.data.map(i => (
            <ListItem selected key={i.id} onPress={() => props.onPress(i.id)}>
              <Left>
                <Text style={styles.text}>{i.name}</Text>
              </Left>
              <Right>
                <Text style={styles.text}>
                  >
                </Text>
              </Right>
            </ListItem>
          ))}
        </List>
      </Content>
    </Container>
  )
}

export default ListTeams
