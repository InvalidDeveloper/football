import React from 'react'
import { Container, Content, ListItem, Text, Separator, Left, Body } from 'native-base'

const TeamList = (props) => {
  return (
    <Container>
      <Content>
        <Separator bordered>
          <Text>Team</Text>
        </Separator>
        {
          props.squad.map(i => (
            <ListItem key={i.id}>
              <Text>{i.name}</Text>
            </ListItem>
          ))
        }
        <Separator bordered>
          <Text>Matches</Text>
        </Separator>
        {
          props.matches.map(i => (
            <ListItem key={i.id}>
              <Left>
                <Text style={{fontSize: 14}}>{i.competition.name}</Text>
              </Left>
              <Body>
                <Text style={{fontSize: 12}}>{i.homeTeam.name}</Text>
                <Text note style={{fontSize: 10}}>{new Date(i.utcDate).toLocaleString()}</Text>
              </Body>
            </ListItem>
          ))
        }
      </Content>
    </Container>
  )
}

export default TeamList
