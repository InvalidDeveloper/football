import { all, takeLatest } from 'redux-saga/effects'
import { navigate, reset, goBack } from './navigationSaga'
import { login, logout } from './userActions'
import { getTeams } from './footballActions'
import { getTeamInfo } from './teamActions'
import { showToast } from './toastActions'

import { NavigationTypes } from 'Redux/reducers/navigatorReducer'
import { UserTypes } from 'Redux/reducers/userReducer'
import { FootballTypes } from 'Redux/reducers/footballReducer'
import { TeamTypes } from 'Redux/reducers/teamReducer'

import API from '../../services/api'

const api = API.create()

export default function* root() {
  yield all([
    takeLatest(NavigationTypes.NAVIGATE, navigate),
    takeLatest(NavigationTypes.RESET, reset),
    takeLatest(NavigationTypes.GO_BACK, goBack),

    takeLatest(UserTypes.LOGIN, login),
    takeLatest(UserTypes.LOGOUT, logout),

    takeLatest(FootballTypes.GET_TEAMS, getTeams, api),
    takeLatest(FootballTypes.GET_TEAMS_ERROR, showToast),

    takeLatest(TeamTypes.GET_TEAM_INFO, getTeamInfo, api),
    takeLatest(TeamTypes.GET_TEAM_INFO_ERROR, showToast),

  ])
}
