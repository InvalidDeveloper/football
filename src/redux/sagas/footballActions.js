import { put, call } from 'redux-saga/effects'
import { FootballActions } from 'Redux/reducers/footballReducer'

export function* getTeams(api, action) {
  try {
    const response = yield call(api.teams)
    if (response.ok) {
      yield put(
        FootballActions.getTeamsSuccess(response.data),
      )
    } else {
      throw new Error(response.data.message)
    }
  } catch (error) {
    yield put(
      FootballActions.getTeamsError({
          error: error.message,
          isError: true,
        }
      ),
    )
  }
}

