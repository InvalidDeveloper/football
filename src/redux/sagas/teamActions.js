import { put, call, all } from 'redux-saga/effects'
import { TeamActions } from 'Redux/reducers/teamReducer'
import { NavigationActions } from 'Redux/reducers/navigatorReducer'
import { TEAM_SCREEN } from 'Consts/screenNames'
import dateFormat from 'dateformat'

export function* getTeamInfo(api, action) {
  try {
    const date = new Date()
    const [matches, squad] = yield all([
      call(api.teamById, action.payload),
      call(api.matchesById, {
        params: {
          limit: 10,
          dateFrom: dateFormat(date, 'isoDate'),
          dateTo: dateFormat(new Date(date.setFullYear(date.getFullYear() + 1)), 'isoDate'),
        },
        id: action.payload
      })])
    if (matches.ok && squad.ok) {
      yield put(
        TeamActions.getTeamInfoSuccess({squad: matches.data.squad, matches: squad.data.matches})
      )
      yield put(
        NavigationActions.navigate({
          routeName: TEAM_SCREEN,
        }),
      )
    } else {
      throw new Error(matches.data.message || squad.data.message)
    }
  } catch (error) {
    yield put(
      TeamActions.getTeamInfoError({
        error: error.message,
        isError: true,
      })
    )
  }
}

