import { combineReducers } from 'redux'
import { createNavigationReducer } from 'react-navigation-redux-helpers'
import AppNavigator from '../../navigator/index'
import { reducer as userReducer } from './userReducer'
import { reducer as footballReducer } from './footballReducer'
import { reducer as teamReducer } from './teamReducer'

const navigatorReducer = createNavigationReducer(AppNavigator)

export default combineReducers({
  navigatorReducer: navigatorReducer,
  userReducer,
  footballReducer,
  teamReducer,
})
