import { createActions, createReducer } from 'reduxsauce'

const initialState = {
  matches: [],
  squad: [],
  loading: false,
  error: '',
}

const {Types, Creators} = createActions({
  getTeamInfoSuccess: ['payload'],
  getTeamInfoError: ['payload'],
  getTeamInfo: ['payload'],
  logout: [],
})

const getTeamInfo = (state) => {
  return {
    ...state,
    loading: true,
    error: '',
  }
}

const getTeamInfoSuccess = (state, action) => {
  return {
    ...state,
    squad: action.payload.squad,
    matches: action.payload.matches,
    error: '',
    loading: false,
  }
}
const getTeamInfoError = (state, action) => {
  console.log(action)
  return {
    ...state,
    error: action.payload.error,
    loading: false,
  }
}
export const LOGOUT = () => initialState

export const reducer = createReducer(initialState, {
  [Types.GET_TEAM_INFO]: getTeamInfo,
  [Types.GET_TEAM_INFO_SUCCESS]: getTeamInfoSuccess,
  [Types.GET_TEAM_INFO_ERROR]: getTeamInfoError,
  LOGOUT,
})

export { Types as TeamTypes, Creators as TeamActions }
