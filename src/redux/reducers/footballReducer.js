import { createActions, createReducer } from 'reduxsauce'

const initialState = {
  teams: [],
  loading: false,
  error: '',
}

const {Types, Creators} = createActions({
  getTeams: [],
  getTeamsSuccess: ['payload'],
  getTeamsError: ['payload'],
  logout: [],
})

const getTeams = (state) => {
  return {
    ...state,
    loading: true,
    error: '',
  }
}
const getTeamsSuccess = (state, action) => {
  return {
    ...state,
    teams: action.payload.teams,
    loading: false,
    error: '',
  }
}
const getTeamsError = (state, action) => {
  return {
    ...state,
    loading: false,
    error: action.payload.error,
  }
}

export const LOGOUT = () => initialState

export const reducer = createReducer(initialState, {
  [Types.GET_TEAMS]: getTeams,
  [Types.GET_TEAMS_SUCCESS]: getTeamsSuccess,
  [Types.GET_TEAMS_ERROR]: getTeamsError,
  LOGOUT,
})

export { Types as FootballTypes, Creators as FootballActions }
